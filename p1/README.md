> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Web App Development

## Allyson Davis
### Project #1 Requirements:

1. Include app splash screen with title
2. Include album cover images and media
3. Create buttons that will play and pause artists' media
4. Create and displaylauncher icon image
5. Provide screenshots of application and skillsets


#### README.md file should include the following items:

* Screenshot of splash screen
* Screenshot of home page
* Screenshot of button pushed - music playing
* Screenshots of skillsets 7, 8, & 9


>
>
>


#### Assignment Screenshots:
* This music application shows three different album covers with buttons that play the song corresponding to the picture. Each of the buttons will play the song when pressed and pause when pressed again.


| *My Music App Splash Screen*:      | *Music App Home*:    | *Music Playing*:	| *Music App w/ splash screen*:	|
| :------------- | :-----------: | :-----------: |-----------: |
|  ![music app splash screen](img/splash.PNG) | ![music app home](img/home.PNG)    | ![music playing](img/playing.PNG) | ![music app](img/app.gif) |


#### Skillset Screenshots:

*Screenshot of Skillset 7 - Measurement Conversion*: 
![skillset7](img/ss7.PNG) 

| *Screenshot of Skillset 8 - Triangle GUI*:      | | 
| :------------- | -----------: |
|  ![skillset8](img/ss8-1.PNG) | ![skillset8](img/ss8-2.PNG)    |

| *Screenshot of Skillset 9 - Multiple Selection List GUI*:      | | |
| :------------- | :-----------: |  -----------: |
|  ![skillset9](img/ss9-1.PNG) | ![skillset9](img/ss9-2.PNG)    | ![skillset9](img/ss9-3.PNG) |

