> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Web App Development

## Allyson Davis
### Assignment #1 Requirements:

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Git command descriptions
4. My First App
5. Contacts App
6. Bitbucket repo links: a) this assignment and b) completed tutorials (bitbucketstationlocations and myteamquotes).


#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands with short descriptions
* Screenshots of Contacts app
* Bitbucket tutorial link (bitbucketstationlocations).


>
> #### Git commands w/short descriptions:

1. git init: creates an empty git repository or reinitializes an existing one

2. git status: displays the state of the working directory and lets you see which changes have been staged, which haven't, and which files aren't being tracked

3. git add: adds a change in the working directory to the staging area

4. git commit: used to save changes to the local Git repository

5. git push: used to upload local repository content to a remote repository

6. git pull: used to fetch and download content from a remote repository and update the local repository to match that content

7. One additional command - git clone: used to copy a git repository from a remote source and sets the remote to original source so you can pull again


#### Assignment Screenshots:


| *Screenshot of running java Hello*:      | *Screenshot of Android Studio - My First App*:    |
| :------------- | -----------: |
|  ![JDK Installation Screenshot](img/jdk_install.PNG) | ![Android Studio Installation Screenshot](img/myfirstapp.PNG)    |


| *Screenshot of Contacts App Home*:      | *Screenshot of First Contact*:    | *Contacts App*:		|
| :------------- | :-----------: | -----------: |
|  ![Contacts App Home](img/home.PNG) | ![First Contact Screen](img/contact1.PNG)    | ![Contacts Application](img/contactsApp.gif) |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/aed18c/bitbucketstationlocations/ "Bitbucket Station Locations")
