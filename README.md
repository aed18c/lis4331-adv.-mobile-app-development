
# LIS4331 Advanced Mobile Web App Development

## Allyson Davis

### Assignment Requirements:

*Assignment Links :*

1. [A1 README.md](a1/README.md "My A1 README.md file")

      - Install Java
      - Install Android Studio and create My First App  
      - Create Contacts App
      - Create Bitbucket repo
      - Complete Bitbucket Tutorials (bitbucketstationlocations and myteamquotes)
      - Provide git command descripteds
      - Provide screenshots of installations

2. [A2 README.md](a2/README.md "My A2 README.md file")
   
      - Research drop-down menus in Android Studio
      - Add Background color/ theme
      - Create launcher icon
      - Create Tip Calculator App
      - Provide screenshots of application and skillsets

3. [A3 README.md](a3/README.md "My A3 README.md file")

      - Include toast notification if user enters out-of-range values
      - Include radio buttons to convert from U.S. to Euros, Pesos and Canadian currency
      - Must include correct sign for euros, pesos, and Canadian dollars
      - Create and displaylauncher icon image
      - Create Splash/Loading Screen
      - Provide screenshots of application and skillsets

4. [A4 README.md](a4/README.md "My A4 README.md file")

      - Include app splash screen with title
      - Include appropriate images on the screens
      - Use persistent data: SharedPreferences
      - Create and displaylauncher icon image
      - Provide screenshots of application and skillsets

5. [A5 README.md](a5/README.md "My A5 README.md file")

      - Include app splash screen with title
      - Include a main screen with app title and list of articles
      - Use your own RSS feed
      - Create and display launcher icon image
      - Provide screenshots of application and skillsets

### Project Requirements:

*Project Links :*

1. [P1 README.md](p1/README.md "My P1 README.md file")

      - Include app splash screen with title
      - Include album cover images and media
      - Create buttons that will play and pause artists' media
      - Create and displaylauncher icon image
      - Provide screenshots of application and skillsets

2. [P2 README.md](p2/README.md "My P2 README.md file")

      - Include app splash screen with title
      - Use persistent data: SQLite database
      - Insert at least five users
      - Create and display launcher icon image
      - Provide screenshots of application


