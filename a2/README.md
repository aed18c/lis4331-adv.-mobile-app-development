> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Web App Development

## Allyson Davis
### Assignment #2 Requirements:

1. Research drop-down menus in Android Studio
2. Add Background color/ theme
3. Create launcher icon
4. Create Tip Calculator App
5. Provide screenshots of application and skillsets


#### README.md file should include the following items:

* Screenshot of unpopulated  user interface
* Screenshot of populated user interface
* Screenshots of skillsets 1, 2, & 3


>
>
>


#### Assignment Screenshots:


| *Tip Calculator App Home*:      | *Tip Calculation*:    | *Tip Calculator App*:		|
| :------------- | :-----------: | -----------: |
|  ![tip calculator home](img/home.PNG) | ![tip calculation](img/calculate.PNG)    | ![tip calculator app](img/app.gif) |


| *Screenshot of Skillset 1 - Circle*:      | *Screenshot of Skillset 2 - MultipleNumbers*:    | *Screenshot of Skillset 3 - TestScores*:		|
| :------------- | :-----------: | -----------: |
|  ![skillset1](img/ss1.PNG) | ![skillset2](img/ss2.PNG)    | ![skillset3](img/ss3.PNG) |