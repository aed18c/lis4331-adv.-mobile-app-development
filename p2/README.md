> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Web App Development

## Allyson Davis
### Project# 2 Requirements:

1. Include app splash screen with title
2. Use persistent data: SQLite database
3. Insert at least five users
4. Create and display launcher icon image
5. Provide screenshots of application


#### README.md file should include the following items:

* Screenshot of splash screen
* Screenshot of add, update, and delete functions
* Screenshot of view data

>
>
>


#### Assignment Screenshots:

| *Users App w/ splash screen*: | *Users App Splash Screen*: | *Home Activity*: |
| :------------- | :-----------: |-----------: | 
|  ![users app](img/app.gif) | ![users app splash screen](img/splash.PNG)    | ![users home](img/main.PNG) |

| *Users App Add*: | *Users App Update*: | *Users App View*: | *Users App Delete*: |
| :------------- | :-----------: | :-----------: |-----------: | 
|  ![users app add](img/add.PNG) | ![users app update](img/update.PNG)    | ![users app view](img/view.PNG) | ![users app delete](img/delete.PNG) |

#### Dive Pics:

| *USS Strength Videos (4/8/21)*:|  |
| :------------- | -----------: |
|  ![uss strength1](dive/strength1.gif) | ![uss strength2](dive/strength2.gif) |

| *Jackson Blue Springs - Marianna, FL (3/4/21)*:|  |  |  
| :------------- | :-----------: | -----------: |
|  ![jackson blue](dive/blue1.jpeg) | ![jackson blue](dive/blue2.jpeg) | ![jackson blue](dive/blue4.jpeg) |

| *Morrison Springs - Ponce De Leon (4/1/21)*:|  |  
| :------------- | -----------: |
|  ![morrison springs](dive/morrison3.jpeg) | ![morrison springs](dive/morrison2.jpeg) |

| |  |
| :------------- | -----------: |
| ![morrison springs](dive/morrison4.jpeg) | ![morrison springs](dive/morrison1.jpeg) |