> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Web App Development

## Allyson Davis
### Assignment # 5 Requirements:

1. Include app splash screen with title
2. Include a main screen with app title and list of articles
3. Use your own RSS feed
4. Create and display launcher icon image
5. Provide screenshots of application and skillsets


#### README.md file should include the following items:

* Screenshot of splash screen
* Screenshot of RSS feed page
* Screenshot of RSS Item
* Screenshot of Read More
* Screenshots of skillsets 13, 14, & 15


>
>
>


#### Assignment Screenshots:

| *RSS App w/ splash screen*: | *RSS Feed Home*: | *Item Activity*: | *Read More...* |
| :------------- | :-----------: | :-----------: |-----------: | 
|  ![rss feed app](img/app.gif) | ![rss feed home](img/rss.PNG)    | ![rss item](img/item.PNG) | ![read more](img/read.PNG) |


#### Skillset Screenshots:

| *Screenshot of Skillset 13*:| *Screenshot of Skillset 15*: |
| :------------- | -----------: |
|  ![skillset13](img/ss13.PNG) | ![skillset15](img/ss15.PNG)    |

*Screenshot of Skillset 14*:

![skillset14](img/ss14.PNG)