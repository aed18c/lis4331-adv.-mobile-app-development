> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Web App Development

## Allyson Davis
### Assignment #3 Requirements:

1. Include toast notification if user enters out-of-range values
2. Include radio buttons to convert from U.S. to Euros, Pesos and Canadian currency
3. Must include correct sign for euros, pesos, and Canadian dollars
4. Create and displaylauncher icon image
5. Create Splash/Loading Screen
6. Provide screenshots of application and skillsets


#### README.md file should include the following items:

* Screenshot of unpopulated  user interface
* Screenshot of populated user interface
* Screenshot of toast notification
* Screenshots of skillsets 4, 5, & 6


>
>
>


#### Assignment Screenshots:


| *Currency Converter App Home*:      | *Converted Currency*:    | *Toast Notification*:	| *Currency Converter App w/ splash screen*:	|
| :------------- | :-----------: | :-----------: |-----------: |
|  ![currency converter home](img/home.PNG) | ![currency converted](img/converted.PNG)    | ![toast notification](img/toast.PNG) | ![tip calculator app](img/app.gif) |


#### Skillset Screenshots:

*Screenshot of Skillset 4 - Time Conversion*:
![skillset4](img/ss4.PNG)

| *Screenshot of Skillset 5 - Even or Odd GUI*:      | | | |
| :------------- | :-----------: | :-----------: | -----------: |
|  ![skillset5](img/ss5-1.PNG) | ![skillset5](img/ss5-2.PNG)    | ![skillset5](img/ss5-3.PNG) | ![skillset5](img/ss5-4.PNG) |

| *Screenshot of Skillset 5 - w/ Text Feilds and Label Components*:      | | |
| :------------- | :-----------: |  -----------: |
|  ![skillset5](img/ss5-2-1.PNG) | ![skillset5](img/ss5-2-2.PNG)    | ![skillset5](img/ss5-2-3.PNG) |


| *Screenshot of Skillset 6 - Paint Calculator GUI*:      | | | |
| :------------- | :-----------: | -----------: |
|  ![skillset6](img/ss6-1.PNG) | ![skillset6](img/ss6-2.PNG)    | ![skillset6](img/ss6-3.PNG) |
|  ![skillset6](img/ss6-4.PNG) | ![skillset6](img/ss6-5.PNG)    | ![skillset6](img/ss6-6.PNG) |