//Allyson Davis
//lis4331 skillset 9
//MultipleSelectionListDemo.java

import javax.swing.JFrame;

public class MultipleSelectionListDemo {
    public static void main(String[] args) {
        MultipleSelectionFrame multipleSelectionFrame = new MultipleSelectionFrame();
        multipleSelectionFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        multipleSelectionFrame.setSize(450, 150);
        multipleSelectionFrame.setVisible(true);
    }
}
