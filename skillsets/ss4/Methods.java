//Allyson Davis
//lis4331 skillset4
//Methods.java

import java.util.Scanner;

public class Methods {
    public static void getRequirements(){
        System.out.println("Developer: Allyson Davis");
        System.out.println("Program converts seconds to minutes, hours, days, weeks, and (regular) years--365 days.");
        System.out.println("***Notes***:");
        System.out.println("1) Use integer for seconds (must validate integer input).");
        System.out.println("2) Use printf() function to print (format values per below output).");
        System.out.println("3) Create Java \"constants\" for the following values:");
        System.out.println("\tSECS_IN_MINS,\n\tMINS_IN_HR,\n\tHRS_IN_DAY,\n\tDAYS_IN_WEEK,\n\tDAYS_IN_YR (365 days)\n");
    }//end getRequirements

    public static void validateUserInput() {
        Scanner scnr = new Scanner(System.in);
        int seconds = 0;

        System.out.print("Please enter number of seconds: ");
        while(!scnr.hasNextInt()) {
            System.out.println("Not valid integer!\n");
            scnr.next();
            System.out.print("Please enter number of seconds: ");
        }//end while
        seconds = scnr.nextInt();

        convertSeconds(seconds);

        scnr.close();
    }//end validateUserInput

    public static void convertSeconds(int sec) {
        //declare constant vars
        final int SECS_IN_MINS = 60;
        final int MINS_IN_HR = 60;
        final int HRS_IN_DAY = 24;
        final int DAYS_IN_WEEK = 7;
        final int DAYS_IN_YEAR = 365;
        //declare other vars
        double mins = 0.0;
        double hrs = 0.0;
        double days = 0.0;
        double weeks = 0.0;
        double years = 0.0;

        mins = sec / SECS_IN_MINS;
        hrs = mins / MINS_IN_HR;
        days = hrs / HRS_IN_DAY;
        weeks = days / DAYS_IN_WEEK;
        years = days / DAYS_IN_YEAR;

        printResults(sec, mins, hrs, days, weeks, years);
    }//end convertSeconds

    public static void printResults(int secs, double mins, double hrs, double days, double weeks, double years){
        System.out.printf("%d second(s) equals\n\n", secs);
        System.out.printf("%.2f minute(s)\n", mins);
        System.out.printf("%.3f hour(s)\n", hrs);
        System.out.printf("%.4f day(s)\n", days);
        System.out.printf("%.5f week(s)\n", weeks);
        System.out.printf("%.6f year(s)\n", years);
    }
}//end class
