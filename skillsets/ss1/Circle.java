//Allyson Davis
//lis4331 - skillset 1
//Circle.java
import java.util.Scanner;

public class Circle {
    public static void main(String[] args) {
        System.out.println("Developer: Allyson Davis");
        System.out.println("Non-OOP program calculates diameter, circumference, and circle area.");
        System.out.println("Must use Java's built-in PI constant, printf(), and formatted to 2 decimal places");
        System.out.println("Must *only* permit numeric entry\n");

        Scanner scnr = new Scanner(System.in);
        double radius = 0.0;
        double diameter = 0.0, circumference = 0.0, area = 0.0;

        System.out.print("Enter circle radius: ");
        while(!scnr.hasNextDouble()) {
            System.out.println("Not valid integer!\n");
            scnr.next();
            System.out.print("Please try again. Enter circle radius: ");
        }//end while
        radius = scnr.nextDouble();

        diameter = radius * 2.0;
        circumference = 2 * Math.PI * radius;
        area = Math.PI * radius * radius;

        System.out.printf("\nCircle diameter:  %.2f\nCircumference: %.2f\nArea: %.2f\n", diameter, circumference, area);

    }// end main
}// end class