//Allyson Davis
//lis4331 - skillset 2
//MultipleNumber.java

import java.util.Scanner;

public class MultipleNumber {
    public static void main(String[] args) {
        System.out.println("Developer: Allyson Davis");
        System.out.println("Program detremines if the first number is multiple of second, prints result.");
        System.out.println("Example: 2, 4, 6, 8 are multiples of 2.");
        System.out.println("1) Use integers. 2) Use printf() function to print.");
        System.out.println("Must *only permit integer entry.\n");

        Scanner scnr = new Scanner(System.in);

        int num1 = 0; 
        int num2 = 0;
        int valid = 0; 
        int product = 0;

        System.out.print("Num 1: ");
        while(!scnr.hasNextInt()) {
            System.out.println("Not  valid integer!\n");
            scnr.next();
            System.out.print("Please try again. Enter Num 1: ");
        }//end while num1
        num1 = scnr.nextInt();

        System.out.print("Num 2: ");
        while(!scnr.hasNextInt()) {
            System.out.println("Not  valid integer!\n");
            scnr.next();
            System.out.print("Please try again. Enter Num 2: ");
        }//end while num1
        num2 = scnr.nextInt();

        valid = num1 % num2;

        System.out.println();

        if (valid == 0) {
            product = num1/num2;
            System.out.printf("%d is a multiple of %d\n", num1, num2);
            System.out.printf("The product of %d and %d is %d", product, num2, num1);
        }
        else 
            System.out.printf("%d is not a multiple of %d", num1, num2);

        System.out.println();
    }//end main
}//end class