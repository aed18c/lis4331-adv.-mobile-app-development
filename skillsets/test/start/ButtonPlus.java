package start;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ButtonPlus implements ActionListener{
    static JButton A;
    static JButton B;
    static JButton C;

    public void planWindow() {
        JFrame frm = new JFrame("Paint Decision");
        JPanel panel = new JPanel();
        frm.add(panel);

        //buttons
        A = new JButton("Choice A");
        B = new JButton ("Choice B");
        C = new JButton ("Choice C");

        A.addActionListener(this);
        B.addActionListener(this);
        C.addActionListener(this);

        panel.add(A);
        panel.add(B);
        panel.add(C);
        frm.setSize(300,100);
        frm.setLocationRelativeTo(null);
        frm.setVisible(true);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }//end planWindow

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == A) {
            JLabel label1 = new JLabel("A is Blue");
        }
        else if (e.getSource() == B) {
            JLabel label1 = new JLabel("B is Red");
        }
        else if (e.getSource() == C) {
            JLabel label1 = new JLabel("C is Yellow");
        }
    }//end actionPerformed

    public static void main(String[] args) {
        ButtonPlus frame = new ButtonPlus();
        frame.planWindow();
    }
}
