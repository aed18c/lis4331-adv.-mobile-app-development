//Allyson Davis
//lis4331 - skillset 3
//Methods.java

import java.util.Scanner;

public class Methods {
    public static void getRequirements() {
        System.out.println("Developer: Allyson Davis");
        System.out.println("Program counts, totals, and averages total number of user-entered exam scores.");
        System.out.println("Please enter exam scores between 0 and 100, inclusive");
        System.out.println("Enter out of range number to end program.");
        System.out.println("Must *only* permit numeric entry.\n");
    }//end getRequirements

    public static void validateUserInput(){
        Scanner scnr = new Scanner (System.in);
        double score = 0.0;
        int count = 0;
        double total = 0.0;

        while (score >= 0.0 && score <= 100.0){

            System.out.print("Enter exam score: ");
            while(!scnr.hasNextDouble()) {
                System.out.println("Not valid number!\n");
                scnr.next();
                System.out.print("Please try again. Enter exam score: ");
            }//end while
            score = scnr.nextDouble();
            if (score >= 0 && score <= 100){
                count ++;
                total = total + score;
            }
        }
        scnr.close();
        printResults(count, total);
    }//end validateUserInput

    public static void printResults(int count, double total) {
        System.out.printf("Count: %d\nTotal: %.2f\nAverage: %.2f\n", count, total, (total/count));
    }//end printResults

}