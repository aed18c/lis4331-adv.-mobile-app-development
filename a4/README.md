> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Web App Development

## Allyson Davis
### Assignment # 4 Requirements:

1. Include app splash screen with title
2. Include appropriate images on the screens
3. Use persistent data: SharedPreferences
4. Create and display launcher icon image
5. Provide screenshots of application and skillsets


#### README.md file should include the following items:

* Screenshot of splash screen
* Screenshot of home page
* Screenshot of Invalid years screen
* Screenshots of skillsets 10, 11, & 12


>
>
>


#### Assignment Screenshots:

| *Interest Calculator App w/ splash screen*: |
| :-------------:| 
| ![interest calculator app](img/app.gif) |

| *Interest Calculator App Splash Screen*:      | *App Home*:    | *Invalid Years*:	| *Calculated Interest*:	|
| :------------- | :-----------: | :-----------: |-----------: |
|  ![interest calculator splash screen](img/splash.PNG) | ![interest calculator home](img/home.PNG)    | ![invalid years entered](img/error.PNG) | ![interest calculator app](img/total.PNG) |


#### Skillset Screenshots:

| *Screenshot of Skillset 10*:| *Screenshot of Skillset 11*: |
| :------------- | :-----------: |  -----------: |
|  ![skillset9](img/ss10.PNG) | ![skillset9](img/ss11.PNG)    |

| *Screenshot of Skillset 12*: |
| :-------------:| 
| ![skillset9](img/ss12.PNG) |

